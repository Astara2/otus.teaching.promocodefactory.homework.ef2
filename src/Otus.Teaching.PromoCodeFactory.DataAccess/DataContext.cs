﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
    public class DataContext: DbContext
    {
        public DbSet<Employee> Employees { get; set; }

        public DbSet<Preference> Preferences { get; set; }

        public DbSet<Role> Roles { get; set; }

        public DbSet<PromoCode> PromoCodes { get; set; }

        public DbSet<Customer> Customers { get; set; }

        public DbSet<CustomerPreference> CustomerPreferences { get; set; }

        public DataContext()
        {

        }

        public DataContext(DbContextOptions<DataContext> options) : base(options)
        { 
        
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Сотрудники

            modelBuilder.Entity<Employee>()
                .ToTable("EmployeeTbl").HasKey(i => i.Id);
            modelBuilder.Entity<Employee>()
                 .Property(i => i.FirstName).IsRequired().HasMaxLength(200);
            modelBuilder.Entity<Employee>()
                 .Property(i => i.LastName).IsRequired().HasMaxLength(200);
            modelBuilder.Entity<Employee>()
                 .Property(i => i.Email).HasMaxLength(100);

            // Клиенты

            modelBuilder.Entity<Customer>()
                .ToTable("CustomerTbl").HasKey(i => i.Id);
            modelBuilder.Entity<Customer>()
                 .Property(i => i.FirstName).IsRequired().HasMaxLength(200);
            modelBuilder.Entity<Customer>()
                 .Property(i => i.LastName).IsRequired().HasMaxLength(200);
            modelBuilder.Entity<Customer>()
                .Property(i => i.Email).HasMaxLength(100);

            // Промокоды
            modelBuilder.Entity<PromoCode>()
                .ToTable("PromoCodeTbl").HasKey(i => i.Id);

            // Роли
            modelBuilder.Entity<Role>()
               .ToTable("RoleTbl").HasKey(i => i.Id);
            modelBuilder.Entity<Role>()
                .Property(i => i.Name).HasMaxLength(100);
            modelBuilder.Entity<Role>()
                .Property(i => i.Description).HasMaxLength(200);

            // Предпочтения
            modelBuilder.Entity<Preference>()
               .ToTable("PreferenceTbl").HasKey(i => i.Id);
            modelBuilder.Entity<Preference>()
                .Property(i => i.Name).IsRequired().HasMaxLength(100);

            // Предпочтения клиентов (многие-ко-многим)
            modelBuilder.Entity<CustomerPreference>()
                .HasKey(bc => new { bc.CustomerId, bc.PreferenceId });
            
            modelBuilder.Entity<CustomerPreference>()
                .HasOne(bc => bc.Customer)
                .WithMany(b => b.Preferences)
                .HasForeignKey(bc => bc.CustomerId);
            
            modelBuilder.Entity<CustomerPreference>()
                .HasOne(bc => bc.Preference)
                .WithMany()
                .HasForeignKey(bc => bc.PreferenceId);

            // Связь клиент-промокод
            modelBuilder.Entity<PromoCode>()
                .HasOne(p => p.Customer)
                .WithMany(c => c.Promocodes)
                .HasForeignKey(bc => bc.CustomerId);
        }
    }
}
