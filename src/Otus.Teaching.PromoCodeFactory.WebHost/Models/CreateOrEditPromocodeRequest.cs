﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;


namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class CreateOrEditPromocodeRequest
    {
        public string ServiceInfo { get; set; }

        public string PartnerName { get; set; }

        public Preference Preference { get; set; }

        public Customer Customer { get; set; }
    }
}