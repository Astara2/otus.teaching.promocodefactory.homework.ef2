﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private readonly IRepository<PromoCode> _promocodeRepository;

        private readonly IRepository<CustomerPreference> _customerPrefRepository;

        private readonly IRepository<Preference> _preferenceRepository;

        public PromocodesController(IRepository<PromoCode> promocodeRepository, 
            IRepository<CustomerPreference> customerRepository, IRepository<Preference> preferenceRepository)
        {
            _promocodeRepository = promocodeRepository;
            _customerPrefRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }

        /// <summary>
        /// Получить все промокоды
        /// </summary>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            var promocodes = await _promocodeRepository.GetAllAsync();

            var promocodesResponse = promocodes.Select(x =>
                new PromoCodeShortResponse()
                {
                    Id = x.Id,
                    Code = x.Code,
                    ServiceInfo = x.ServiceInfo,
                    BeginDate = x.BeginDate.ToString(),
                    EndDate = x.EndDate.ToString(),
                    PartnerName = x.PartnerName
                }).ToList();

            return promocodesResponse;
        }

        /// <summary>
        /// Создать клиента
        /// </summary>
        [HttpPost]
        public async Task<IActionResult> CreatePromocodeAsync(CreateOrEditPromocodeRequest request)
        {
            var promocode = new PromoCode
            {
                Id = new Guid(),
                PartnerName = request.PartnerName,
                ServiceInfo = request.ServiceInfo,
                CustomerId = request.Customer.Id,
                Customer = request.Customer,
                Preference = request.Preference
            };

            await _promocodeRepository.AddAsync(promocode);

            return Ok(promocode);
        }

        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            // Находим клиентов с указанным предпочтением
            var preferenceId = Guid.Parse(request.Preference);
            var customersPreferences = await _customerPrefRepository.GetAllAsync();
            var filteredList = customersPreferences.ToList().Where(i => i.PreferenceId == preferenceId)
                .Select(j => j.Customer);

            var preference = await _preferenceRepository.GetByIdAsync(preferenceId);

            if (preference == null)
                return NotFound();

            // Для каждого клиента делаем запись в таблице промокодов
            foreach (Customer customer in filteredList)
            {
                var createPromoRequest = new CreateOrEditPromocodeRequest
                {
                    ServiceInfo = request.ServiceInfo,
                    PartnerName = request.ServiceInfo,
                    Preference = preference,
                    Customer = customer
                };

                await CreatePromocodeAsync(createPromoRequest);
            }

            return Ok();
        }
    }
}

