﻿using System;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    /// <summary>
    /// Предпочтение клиента
    /// </summary>
    public class CustomerPreference
        : BaseEntity
    {
        public Guid PreferenceId { get; set; }

        public virtual Preference Preference { get; set; }

        public Guid CustomerId { get; set; }

        public virtual Customer Customer { get; set; }
    }
}