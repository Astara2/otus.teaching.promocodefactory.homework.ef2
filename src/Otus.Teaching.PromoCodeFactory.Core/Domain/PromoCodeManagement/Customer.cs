﻿using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Customer
        :BaseEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        public string Email { get; set; }

        /// <summary>
        /// Список предпочтений клиентов
        /// </summary>
        public virtual List<CustomerPreference> Preferences { get; set; }

        /// <summary>
        /// Список предпочтений клиентов
        /// </summary>
        public virtual List<PromoCode> Promocodes { get; set; }

        public string Address { get; set; }
    }
}